/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.auth.controller;

import cn.hutool.core.util.StrUtil;
import com.cloud.common.cache.constants.CacheScope;
import com.cloud.common.cache.util.RedisUtil;
import com.cloud.common.oauth.security.SecurityUser;
import com.cloud.common.util.base.Result;
import com.cloud.common.util.enums.ResultEnum;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Aijm
 * @date 2019/09/06
 * 删除token端点
 */
@RestController
@AllArgsConstructor
@RequestMapping("/token")
public class ProSsoController {

	private final ClientDetailsService clientDetailsService;
	private final TokenStore tokenStore;


    /**
     *  退出登录
	 * @param header
     * @return
     */
	@DeleteMapping("/logout")
	public Result logout(@RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String header) {
		if (StrUtil.isBlank(header)) {
			return Result.error(ResultEnum.LOGOUT_CODE);
		}
		String tokenValue = header.replace(OAuth2AccessToken.BEARER_TYPE, StrUtil.EMPTY).trim();
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
		if (accessToken == null || StrUtil.isBlank(accessToken.getValue())) {
			return Result.error(ResultEnum.TOKEN_ERROR);
		}

		OAuth2Authentication auth = tokenStore.readAuthentication(accessToken);
		SecurityUser user = (SecurityUser)auth.getPrincipal();
		// 清空菜单信息
		RedisUtil.remove(CacheScope.USER_MENU.getCacheName(), user.getUserId().toString());
		// 清空角色信息
		RedisUtil.remove(CacheScope.USER_ROLE.getCacheName(), user.getUserId().toString());
		// 清空用户信息
		RedisUtil.remove(CacheScope.USER_USER.getCacheName(), user.getUserId().toString());
		// 清空access token
		tokenStore.removeAccessToken(accessToken);
		// 清空 refresh token
		OAuth2RefreshToken refreshToken = accessToken.getRefreshToken();
		tokenStore.removeRefreshToken(refreshToken);
		return Result.success("退出登录成功!");
	}



}