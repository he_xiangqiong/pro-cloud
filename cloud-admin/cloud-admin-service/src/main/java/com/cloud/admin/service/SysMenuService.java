/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.service;

import com.cloud.admin.beans.po.SysMenu;
import com.cloud.common.data.base.ITreeService;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author Aijm
 * @since 2019-05-13
 */
public interface SysMenuService extends ITreeService<SysMenu> {

    /**
     *  根据用户id获取到用户的菜单
     * @param userId
     * @return
     */
    List<SysMenu> findByUserId(Long userId);

}