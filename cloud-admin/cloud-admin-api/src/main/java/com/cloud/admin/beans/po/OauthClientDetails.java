/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 *
 * @author Aijm
 * @date 2020-07-11 15:49:54
 */
@Data
@TableName("oauth_client_details")
@ApiModel(description = "")
public class OauthClientDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "客户端Id")
    @TableId
    private String clientId;

    @ApiModelProperty(value = "资源集合")
    private String resourceIds;

    @ApiModelProperty(value = "秘钥")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String clientSecret;

    @ApiModelProperty(value = "范围")
    private String scope;

    @ApiModelProperty(value = "授权类型")
    private String authorizedGrantTypes;

    @ApiModelProperty(value = "重定向")
    private String webServerRedirectUri;

    @ApiModelProperty(value = "")
    private String authorities;

    @ApiModelProperty(value = "访问token有效期")
    private Integer accessTokenValidity;

    @ApiModelProperty(value = "刷新token有效期")
    private Integer refreshTokenValidity;

    @ApiModelProperty(value = "附属信息")
    private String additionalInformation;

    @ApiModelProperty(value = "跳过授权步骤属性")
    private String autoapprove;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;


}